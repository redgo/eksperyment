/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pg.gda.eti.kio.tasks;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mateusz
 */
public class Task3Test {
   
    @Before
    public void setUp() {
        sut = new Task3();
        nonPalindromePhrase = Helpers.GetNonPalindromePhrase();
        palindromePhrase = Helpers.GetPalindromePhrase();
        emptyString = "";
    }
    
    @Test
    public void should_return_false_for_non_palindrome_phrase() {
        System.out.println("Task 3 - Non palindrome test");
        result = sut.IsPalindrome(nonPalindromePhrase);
        assertFalse(result);
    }
    
    @Test
    public void should_return_true_for_palindrome_phrase() {
        System.out.println("Task 3 - Palindrome test");
        result = sut.IsPalindrome(palindromePhrase);
        assertTrue(result);
    }
    
    @Test
    public void should_return_false_for_empty_string() {
        System.out.println("Task 3 - Empty string test");
        result = sut.IsPalindrome(emptyString);
        assertFalse(result);
    }
    
    private String nonPalindromePhrase;
    private String palindromePhrase;
    private String emptyString;
    private Boolean result;
    private Task3 sut;
}
