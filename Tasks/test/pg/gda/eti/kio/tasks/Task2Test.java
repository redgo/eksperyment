/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pg.gda.eti.kio.tasks;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mateusz
 */
public class Task2Test {
    
    @Before
    public void setUp() {
        expected = 55;
        number = 10;
        sut = new Task2();
    }

    @Test
    public void should_calculate_correctly_first_n_fibonacci_numbers() {
        System.out.println("Task 2 - Fibonacci");
        result = sut.Fibonacci(number);
        Helpers.Print(expected, result);
        assertEquals(expected, result);
    }
    
    private int expected;
    private int result;
    private int number;
    private Task2 sut;
}
