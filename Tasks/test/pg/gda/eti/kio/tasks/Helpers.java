/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pg.gda.eti.kio.tasks;

import java.util.Arrays;

/**
 *
 * @author Mateusz
 */
public class Helpers {
    public static int[] GetUnorderedNumbers() {
        return new int[] {1,3,4,2,7,6,5,3,8,0,2,4,6,9,0,5,6,4,2,3,432,57,23,46,3,8563,8,2,234,25,734};
    }
    
    public static int[] GetAscOrderedNumbers() {
        return new int[] {0,0,1,2,2,2,2,3,3,3,3,4,4,4,5,5,6,6,6,7,8,8,9,23,25,46,57,234,432,734,8563};
    }
    
    public static int[] GetDscOrderedNumbers() {
        return new int[] {8563,734,432,234,57,46,25,23,9,8,8,7,6,6,6,5,5,4,4,4,3,3,3,3,2,2,2,2,1,0,0};
    }

    public static String GetNonPalindromePhrase() {
        return "some random phrase";
    }
    
    public static String GetPalindromePhrase() {
        return "anutforajaroftuna";
    }
    
    public static void PrintSortDiff(int[] expected, int[] result) {
        System.out.println("Expected: " + Arrays.toString(expected));
        System.out.println("Result: " + Arrays.toString(result));
    }
    
    public static void Print(int expected, int result) {
        System.out.println("Expected: " + expected);
        System.out.println("Result: " + result);
    }

    public static int[][] GetTransposedMatrix() {
        return new int[][] {
                            { 1,  13, 25 },
                            { 43, -9, 35 },
                            { 15,  0,  3 },
                            { -1, 12, -7 }
                           };
    }

    public static int[][] GetMatrixToTranspose() {
        return new int[][] {
                            { 1, 43, 15, -1 },
                            { 13, -9, 0, 12 },
                            { 25, 35, 3, -7 }
                           };
    }    
}
