/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pg.gda.eti.kio.tasks;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mateusz
 */
public class Task1Test {
    
    @Before
    public void setUp() {
        numbers = Helpers.GetUnorderedNumbers();
        expectedAscNumbers = Helpers.GetAscOrderedNumbers();
        expectedDscNumbers = Helpers.GetDscOrderedNumbers();
        dscOrder = SortOrder.DESCENDING;
        ascOrder = SortOrder.ASCENDING;
        sut = new Task1();
    }

    @Test
    public void should_sort_correctly_if_order_is_ascending() {
        System.out.println("Task 1 - ascending sorting");
        result = sut.Sort(numbers, ascOrder);
        Helpers.PrintSortDiff(expectedAscNumbers, result);
        assertArrayEquals(expectedAscNumbers, result);
    }
    
    @Test
    public void should_sort_correctly_if_order_is_descending() {
        System.out.println("Task 1 - descending sorting");
        result = sut.Sort(numbers, dscOrder);
        Helpers.PrintSortDiff(expectedDscNumbers, result);
        assertArrayEquals(expectedDscNumbers, result);      
    }
    
    private int[] numbers;    
    private int[] expectedAscNumbers;
    private int[] expectedDscNumbers;
    private int[] result;
    private SortOrder dscOrder;
    private SortOrder ascOrder;
    private Task1 sut;
}
