/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pg.gda.eti.kio.tasks;

/**
 *
 * @author Mateusz
 */

/*
Zadanie 1

Uzupełnij funkcjonalność metody tak, aby sortowała liczby algorytmem Bubble Sort
według podanego porządku.

Przykłady

1. dla liczb 1,4,6,2,4 i porządku sortowania ASCENDING (rosnąco)
prawidłowym wynikiem jest zwrócenie tablicy: 1,2,4,4,6

2. dla liczb 1,5,7,2,3,6,4,8 i porządku sortowania DESCENDING (malejąco)
prawidłowym wynikiem jest zwrócenie tablicy: 8,7,6,5,4,3,2,1

Do sprawdzenia poprawności rozwiązania zadania skorzystaj z przygotowanych testów.
(prawy przycisk myszki na plik -> Test File albo kombinacja klawiszy Ctrl+F6)
*/
enum SortOrder {
    ASCENDING,
    DESCENDING
}

public class Task1 {
    public int[] Sort(int[] numbers, SortOrder order) {
        
        /* 
            najprostszym algorytmem sortowania jest sortowanie bąbelkowe
            iterujemy po tablicy i porównujemy dwa sąsiednie elementy,
            jeżeli są różne, to zamieniamy je miejscami
        
        */
        if(/* porządek sortowania malejący*/) {
            for(int i = 0; i < ... ; i++) {
                for(/* uzupełnij */) {
                    // zamień elementy jeśli są różne
                }
            }
        }
        else {
            for(int i = 0; i < ... ; i++) {
                for(/* uzupełnij */) {
                    // zamień elementy jeśli są różne
                }
            }
        }
        
        // zwróć wynik
    }
}
